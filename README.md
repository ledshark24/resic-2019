# Resic 2019

## TL;PL

Présentation donnée lors des RESIC 2019 par le groupe sysinfo.

lancez le fichier `presentation.html` dans votre navigateur.

## Pour construire ce fichier :

* [pandoc](http://pandoc.org)
* curl
* make

donc dans toutes les bonnes distributions

`sudo apt-get install pandoc curl make`

Génération :

c'est pas compliqué,

	cd RESIC-2019
	make clean
	make

Attention ça va télécharger reveal.js depuis github et le dé-tar dans le dossier de travail
