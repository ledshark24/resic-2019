% Web alternatif et décentralisé
% RESIC 2019
% [Équipe sysinfo](mailto:sysinfo@isf-france.org)

# Introduction

# Centralisé, décentralisé

## Internet ça marche comment ?

. . .

_Usager_ 
:	je veux voir `https://www.isf-france.org`

_Usager_ 
:	qui est `www.isf-france.org` ?

_Serveur DNS_ 
:	`www.isf-france.org` a pour adresse `51.77.137.86`

_Usager_ 
:	Allo le `51.77.137.86` ? 

	ici le `1.2.3.4` j'aimerais voir `https://www.isf-france.org`

_www.isf-france.org_ 
:	Allo le `1.2.3.4` ? 

	Voilà la page (attention c'est long)

----

```
<head>
  <link rel="profile" href="http://www.w3.org/1999/xhtml/vocab">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Donner du sens à la technique, pour un monde plus juste. Association française de solidarité internationale créée en 1982, Ingénieurs sans frontières se donne donc pour mission de participer à la construction du développement durable par une pratique critique de la démarche de l’ingénieur.">
<!-- -->
<style>
@import url("https://www.isf-france.org/sites/all/libraries/slick/slick/slick.css?plkq9z");
</style>
<style media="screen">
@import url("https://www.isf-france.org/sites/all/libraries/leaflet_markercluster/dist/MarkerCluster.css?plkq9z");
@import url("https://www.isf-france.org/sites/all/libraries/leaflet_markercluster/dist/MarkerCluster.Default.css?plkq9z");
</style>
<style>
@import url("https://www.isf-france.org/sites/all/modules/date/date_api/date.css?plkq9z");
@import url("https://www.isf-france.org/sites/all/modules/date/date_popup/themes/datepicker.1.7.css?plkq9z");
@import url("https://www.isf-france.org/modules/field/theme/field.css?plkq9z");
```

##  Vous connaissez cet objet ?

![](img/Minitel_terminal.jpg)


[^credit-photo-minitel]: Minitel: Crédit  CC-BY 2.0 [Wikipedia](https://fr.wikipedia.org/wiki/Minitel#/media/File:Minitel_terminal.jpg)

. . .

c'est un minitel [^credit-photo-minitel]

## Dessines moi un réseau

là c'est à vous !

. . . 

![](img/dessin-vide.svg)

# État des lieux

## Internet aujourd'hui

Peu d'acteurs pour la quasi totalité des utilisateurs.

Ces acteurs concentrent les données

... et les pouvoirs.


Ainsi se sont formés les géants du web.


## propriété des données

Le site [Terms of Service Didn't Read](https://tosdr.org/) résume bien les
  CGU de nos sites favoris

. . .

Par exemple [facebook](https://tosdr.org/#facebook) :

. . . 

- utilise votre identité et la diffuse à autrui

. . .

- vous traque sur le reste du vaste internet (les fameux boutons +1)

. . .

- dispose d'un droit d'usage global et illimité des données & contenus

. . .

- y compris à la revente à des tiers

. . .

- le contenu que vous supprimez y est quand même conservé

. . .

- fournit vos données à des tiers

. . .

Mais sinon promis vous restez propriétaires de vos données (c'est dans leurs
TOS)

## si c'est gratuit c'est vous le produit

À _votre_ avis **Qui** paie Facebook pour **Quels** services ?

. . . 

En 2017 Facebook offre ses services à 2 Milliard d'utilisateurs

. . .

Avec un chiffre d'affaire de 40,65 Milliard de dollars

. . .

et un bénéfice 2017 qui lui est de 15,93 Milliard de dollars

. . . 

soit ... ~$8 par usager.

## Contrôle éditorial

À votre avis **QUI** détermine ce qui peut et ne peut pas être publié sur
facebook ?

. . . 

Et sinon "modérateur facebook" c'est un bon métier ?

. . .


----------------

Facebook identifie automagiquement les contenus suivants :

par exemple au 1er trimestre 2018

Spams
: 837 millions, 99% en automatique

Faux comptes
: 583 millions, pas de chiffres sur les automatismes

Pornographie
: 21 millions, 96 % en automatique

	(l'age minimum pour facebook est de _13 ans_)

----------------

Violence, haine
: 3,5 millions, 86% en automatique

Propagande terroriste
: 1.9 millions,
	
	pas de chiffres sur les automatismes

. . .

depuis mai 2018 il est possible de faire "appel" d'un retrait de contenu [^source-moderation-facebook]

[^source-moderation-facebook]: Modération facebook : Article en ligne du [Nouvel Observateur](https://www.nouvelobs.com/societe/20180516.OBS6779/facebook-devoile-sa-moderation-on-y-voit-plus-clair-mais-du-flou-subsiste.html)

## Big Data ou Big Brother ?

La donnée c'est un marché global de 23.8 milliards d'euros en 2016

. . . 

Certains parlent déjà de nouvel or noir

# Alternatives

## Des alternatives ... à construire 

[https://degooglisons-internet.org](https://degooglisons-internet.org)
![](https://degooglisons-internet.org/img/Peha-Banquet-CC-By-1920.jpg)\

## Sondage ?

## OpenSondage

OpenSondage : fortement maintenu par Framasoft et basé sur STUdS logiciel développé par l'Université de Strasbourg

![](img/OpenSondage-screenshot.png)

## Fichiers ?

## Nextcloud

Nextcloud : logiciel libre d'hébergement de fichiers accessible par navigateur web, fork de Owncloud

![](img/Nextcloud-screenshot.png)

## formulaires ?

## webform

Webform : module du CMS libre Drupal pour créer des formulaire

![](img/Formulaire-screenshot.png)

## Discussions ?

----------------------

IRC (Internet Relay Chat), l'ancêtre toujours là :

Protocole de communication textuelle sur Internet, conçu fin août 1988, principalement de la discussion en groupe par canaux de discussion.

![](img/Screenshot_of_HexChat_in_Windows_8.png)

[^credit-photo-HexChat]

[^credit-photo-HexChat]: HexChat: [HexChat](https://hexchat.github.io/screenshots.html), capture d'écran GNU GPL

---------------------

logiciels libre et open source libre de chat en équipe

[Rocket.Chat](https://rocket.chat/) - [Mattermost](https://mattermost.com/)

![](img/RocketChatdevices-small.png)   ![](img/Mattermostdevices-small.png)

[^credit-photo-RocketChat],[^credit-photo-Mattermost]

[^credit-photo-RocketChat]: Rocket.Chat: images extraites du site web [Rocket.Chat](https://rocket.chat/)
[^credit-photo-Mattermost]: Rocket.Chat: images extraites du site web [Mattermost](https://mattermost.com/)

----------------------

[Mumble](https://wiki.mumble.info/wiki/Main_Page)\
![](img/mumble.png)

----------------------

[Jitsi](https://jitsi.org/) - Jitsi Meet

Logiciel libre et open source de conférence vidéo

![](img/jitsi-front.png)

[^credit-photo-Jitsi]

[^credit-photo-Jitsi]: Jitsi: images extraites du site web [Jitsi](https://jitsi.org/)


## Des alternatives ... à héberger

pour aller plus loin dans le degooglisons.


Jusqu'à l'auto-hébergement !

## Yunohost et la Brique Internet

[YunoHost](https://yunohost.org/)
: « Pourquoi ne pas s'héberger? », est une distribution basée sur Debian GNU/Linux composée, essentiellement, de logiciels libres, ayant pour objectif de faciliter l'auto-hébergement.

![](img/YunoHost-logo.png)

-----------

... Et [La brique internet](https://labriqueinter.net/)
: un simple boîtier VPN couplé à un serveur.

![](img/labriqueinternet-schema.png)

## Et si on se regroupait ?

Les CHATONS, AMAP du numérique ?

![](img/chatons.png)

---------------

[CHATONS](https://chatons.org/) est le Collectif des Hébergeurs Alternatifs,Transparents, Ouverts, Neutres et Solidaires.

Il rassemble des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).


## Et ISF ?

![](img/ISF-Services.png)

## Décentraliser l'infrastructure

[FFDN](https://ffdn.org)

Fédération des Fournisseurs d'Accès Internet Associatifs

regroupe une 30aine d'associations de fourniture de services réseau (et plus)

- wifi
- ADSL
- Fibre
- VPN
- Radio
- ...

## Et les moteurs de recherches ?

## YaCy

Le moteur de recherche décentralisé en peer to peer

![](img/YaCy.png)


## Qwant

Le moteur de recherche français qui ne récolte pas nos données

![](img/Qwant_v4.png)

# Fédération

## Définitions

> La redécentralisation d'Internet consiste à mettre les terminaux, les services et l'information à la périphérie du réseau

## Il est vieux il marche bien

. . .

**papy email !**

. . .

en 1972 Ray Tomlinson crée la première adresse `tomlinson@bbn-tenexa` sur
ARPANET.[^source-email]

. . .

![](img/email.png)

[^source-email]: schéma de fonctionnement d'un mail [wikipedia](https://fr.wikipedia.org/wiki/Courrier_%C3%A9lectronique)




## RSS

Format de données utilisés pour la syndication de contenu Web.

Vous pouvez agréger plusieurs flux dans un logiciel ad-hoc

## Communications instantanées

![](img/xmpp-logo.png)

XMPP

[Extensible Messaging and Presence Protocol](https://xmpp.org) : ensemble de protocoles standards ouverts pour (entre autres) la messagerie instantanée, appels vidéo et audio, la collaboration et l'échange décentralisée de données (XML).

Développé à l'origine dans la communauté Open Source Jabber dès 1998 pour fournir une alternative décentralisée aux services de messagerie instantanée fermés de l'époque.

---------------

![](img/matrix-logo.png)

[Matrix](https://matrix.org) : Protocole ouvert pour des communications sécurisées et décentralisées, développé depuis fin 2014

Solution choisie et déployée [en 2018 par l'état français](https://matrix.org/blog/2018/04/26/matrix-and-riot-confirmed-as-the-basis-for-frances-secure-instant-messenger-app/)

![](img/riot-web-featured.png)

[^credit-photo-Riot]

[^credit-photo-Riot]: Matrix-Riot: image du client Riot extraite du site web [Matrix](https://matrix.org)

## ActivityPub : Ça bouge dans tous les sens : les petits nouveaux 

Standard au niveau "recommandation" du 3WC depuis le 20 mars 2018

![](https://activitypub.rocks/static/images/ActivityPub-tutorial-image.png)

-------------------------

## Micro-blog ?

## Mastondon {data-background-Image=img/Mastodon.gif data-background-opacity=0.25}

Mastodon  [^source-mastodon] est à la fois un logiciel libre de microblogage, et un réseau social décentralisé, créé en 2016, qui permet de publier des messages appelés « pouets »

Il existe de nombreuses instances de l'individuelle auto hébergée
à l'instance de plusieurs dizaines de milliers d'utilisateurs en passant par
l'instance de l'administration française (étatlab)

pour en savoir plus : [joinmastodon.org](https://joinmastodon.org/)

[^source-mastodon]: fond de la diapositive : extrait vidéo de [What is Mastodon](https://www.youtube.com/watch?v=IPSbNdBmWKE) CC-By

## Vidéos ?

## Peertube {data-background-Image=img/peertube.png data-background-opacity=0.15}


PeerTube [^source-fond-peertube] est un logiciel libre, d'hébergement de vidéo décentralisé grâce à la diffusion en pair à pair, créé en 2015 et soutenu par Framasoft.

[^source-fond-peertube]: fond de la diapositive : extrait de la vidéo [What is PeerTube](https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d) CC-By

[joinpeertube.org](https://joinpeertube.org/fr/)

## Échanger avec ses amis

## Diaspora\* {data-background-Image=img/Diaspora.svg data-background-opacity=0.15}

Une application web de réseau social décentralisé. La première version publique du code source du logiciel a été diffusée le 15 septembre 2010 


## Peux-t-on _TOUT_ fédérer ?

. . .

Partage d'image

. . .

[PixelFed](https://pixelfed.org/)

. . . 

Blog

. . . 

[plume](https://fediverse.blog/)

. . .

Partage de musique

. . .

[Funkwhale](https://funkwhale.audio/)

. . .

Partage de fichiers

. . .

[bitorrent](https://www.utorrent.com/)

. . .

[Nextcloud](https://nextcloud.com/)

# Des questions ?

## cette présentation

Pour télécharger ce fichier (et ses sources) :

![](img/rqcode.png)

[framagit](https://framagit.org/ledshark24/resic-2019/)
